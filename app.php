<?php

require 'vendor/autoload.php';

use App\Model\Rover;
use App\Model\Coordinate;
use App\Model\Direction;
use App\Model\Commands;
use App\Model\Command;
use App\Model\Planet;
use App\Model\Obstacle;


//Creating Planet 200x200
$maxBoundary = new Coordinate(200,200); 
$minBoundary = new Coordinate(0,0); 
$planet = new Planet($minBoundary, $maxBoundary, "Mars");
echo $planet;


//Creating obstacles
do {
	echo "\nDo you want to place an obstacle? Y/N:\n";
	$input = strtoupper(trim(fgets(STDIN)));

	if ($input == "Y") {
		echo "\nSet X coordinate of obstacle:\n";
		$inputX = trim(fgets(STDIN));
		echo "\nSet Y coordinate of obstacle:\n";
		$inputY = trim(fgets(STDIN));

		$obstacleCoordinates = new Coordinate($inputX,$inputY);
		$obstacle = new Obstacle($obstacleCoordinates); 
		$planet->setObstacle($obstacle);
		echo "\n".$obstacle;
	}
} while ($input == "Y");


//Rover setup
$rover = new Rover($planet);
//Rover coordinates
echo "\nSet X coordinate of Rover:\n";
$inputX = trim(fgets(STDIN));
echo "\nSet Y coordinate of Rover:\n";
$inputY = trim(fgets(STDIN));
$coordinate = new Coordinate($inputX,$inputY);
$rover->setPosition($coordinate);

//Rover direction
echo "\nSet Rover direction [".Direction::NORTH.",".Direction::SOUTH.",".Direction::EAST.",".Direction::WEST."]:\n";
$input = strtoupper(trim(fgets(STDIN)));
$direction = new Direction($input);
$rover->setDirection($direction);

echo "\nActual position: ".$rover;

//Input commands
do {
	echo "\nDo you want to move Rover? Y/N:\n";
	$input = strtoupper(trim(fgets(STDIN)));

	if ($input == "Y") {
		echo "\nWrite commands to move Rover [".Command::FORWARD.",".Command::LEFT.",".Command::RIGHT."]:\n";

		$commands = new Commands();
		$inputCommands = strtoupper(trim(fgets(STDIN)));
		$commands->setCommands($inputCommands);

		//Processing commands
		$rover->processCommands($commands->getCommands());
	}
	
	echo "\nFinished position: ".$rover;
} while ($input == "Y");