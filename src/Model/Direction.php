<?php
declare( strict_types = 1 );

namespace App\Model;

class Direction {

	/**
     * North direction
     *
     * @var string
     */
	const NORTH = 'N';

	/**
     * East direction
     *
     * @var string
     */
	const EAST = 'E';

	/**
     * South direction
     *
     * @var string
     */
	const SOUTH = 'S';

	/**
     * West direction
     *
     * @var string
     */
	const WEST = 'W';

	/**
     * Stores all available directions
     *
     * @var array
     */
	private $availableDirections = [self::NORTH, self::EAST, self::SOUTH, self::WEST];

	/**
     * Stores the given direction string
     *
     * @var string
     */	
	private $direction;

    /**
     * Direction constructor
     * @param string $input
	 * @throws \Exception if the direction is invalid
     */
	public function __construct(string $input)
	{
		if ($this->isValid($input)) {
			$this->direction = $input;
		} else {
			throw new \Exception("Direction: ". $input. " is invalid!");
		}
	}

    /**
     * @return string
     */
	public function __toString(): string
    {
        return $this->direction;
    }

    /**
     * Check if input string is a valid direction
     *
     * @param string $input
     * @return bool
     */
    public function isValid(string $input): bool
    {
    	return in_array($input, $this->availableDirections);
    }
}