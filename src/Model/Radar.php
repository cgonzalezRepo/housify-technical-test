<?php
declare( strict_types = 1 );

namespace App\Model;

class Radar {

    /**
     * Stores a planet
     *
     * @var Planet
     */
	private $planet;

    /**
     * Radar constructor
     * @param Planet $planet
     */
	public function __construct(Planet $planet)
	{
		$this->planet = $planet;
	}

    /**
     * @return string
     */
	public function __toString(): string
    {
        return $this->planet;
    }

    /**
     * Check for obstacles in the given coordinate
     *
     * @param Coordinate $coordinate
     * @return bool
     */
    public function scannForObstacles(Coordinate $coordinate): bool
    {
        $obstacles = $this->planet->getObstacles();

        foreach ($obstacles as $obstacle) {
            if ($obstacle->getPosition() == $coordinate) {
                return true;
            }
        }

    	return false;
    }

    /**
     * Check if the given coordinate its inside planet boundaries
     *
     * @param Coordinate $coordinate
     * @return bool
     */
    public function scannForEdges(Coordinate $coordinate): bool
    {
        if (($coordinate->getX() >= $this->planet->getMinBoundary()->getX() && $coordinate->getX() <= $this->planet->getMaxBoundary()->getX()) && ($coordinate->getY() >= $this->planet->getMinBoundary()->getY() && $coordinate->getY() <= $this->planet->getMaxBoundary()->getY())) {
            return true;
        }
        return false;
    }

}