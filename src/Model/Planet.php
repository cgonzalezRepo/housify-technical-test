<?php
declare( strict_types = 1 );

namespace App\Model;

use App\Model\Coordinate;
use App\Model\Obstacle;

class Planet {

    /**
     * The minimum boundary
     *
     * @var Coordinate
     */
	private $minBoundary;

    /**
     * The maximum boundary
     *
     * @var Coordinate
     */
	private $maxBoundary;

    /**
     * Stores obstacle objects
     *
     * @var array
     */
    private $obstacles = array();

    /**
     * Planet name
     *
     * @var string
     */
    private $name;

    /**
     * Planet constructor
     * @param Coordinate $minBoundary
     * @param Coordinate $maxBoundary
     * @param string $name
     */
	public function __construct(Coordinate $minBoundary, Coordinate $maxBoundary, string $name)
	{
		$this->minBoundary = $minBoundary;
		$this->maxBoundary = $maxBoundary;
        $this->name = $name;
	}

    /**
     * @return string
     */
	public function __toString(): string
    {
        return $this->name. ' has an area of a minimum '.$this->minBoundary . ' and a maximum of '. $this->maxBoundary;
    }

    /**
     * @return Coordinate
     */
    public function getMinBoundary(): Coordinate
    {
        return $this->minBoundary;
    }

    /**
     * @return Coordinate
     */
    public function getMaxBoundary(): Coordinate
    {
    	return $this->maxBoundary;
    }

    /**
     * @param Obstacle $obstacle
     */
    public function setObstacle(Obstacle $obstacle): void
    {
        array_push($this->obstacles, $obstacle);
        return;
    }


    /**
     * @return array
     */
    public function getObstacles(): array
    {
        return $this->obstacles;
    }
}