<?php
declare( strict_types = 1 );

namespace App\Model;

use App\Model\Coordinate;
use App\Model\Direction;
use App\Model\Command;
use App\Model\Radar;

class Rover {

	/**
	 * Movemet unit of Rover repesented as velocity
	 *
	 * @var int
	 */
	CONST VELOCITY = 1;

	/**
	 * Current position
	 *
	 * @var Coordinate
	 */
	private $position;

	/**
	 * Current direction
	 *
	 * @var Direction
	 */
	private $currentDirection;

	/**
	 * Contains the radar
	 *
	 * @var Radar
	 */
	private $radar;

    /**
     * Rover constructor
     * @param Planet $planet
     */
	public function __construct(Planet $planet)
	{
		$this->radar = new Radar($planet);
	}

    /**
     * @return string
     */
	public function __toString(): string
    {
        return 'Coordinate: '.$this->position. ', is facing: '.$this->currentDirection;
    }

    /**
     * Execute the given command and stops if radar finds and obstacle or passes the edge of the planet
     *
     * @param array $commands
	 * @throws \Exception if the given command is invalid
     */
    public function processCommands(array $commands): void
    {
    	foreach ($commands as $command) {
	    	switch ($command) {
	    		case Command::FORWARD:
	    			$targetPosition = $this->checkMovement();
					
					if (!$this->radar->scannForObstacles($targetPosition) && $this->radar->scannForEdges($targetPosition)) {
						$this->move($targetPosition);
					}else {
						echo "Aborting command sequence, found an obstacle at: ". $targetPosition;
						return;
					}
	    			break;  
	    		case Command::LEFT:  
	    			$this->rotateLeft();
	    			break;
	    		case Command::RIGHT:  
	    			$this->rotateRight();
	    			break;
	    		default:
	    			throw new \Exception("Command: ". $command. " is invalid!");
	    			break;
	    	}
    	}
    	return;
    }

    /**
     * Returns the target position that Rover should move
     *
	 * @throws \Exception if the given direction is invalid
     * @return Coordinate
     */
    private function checkMovement(): Coordinate
    {
    	switch ($this->currentDirection) {
    		case Direction::NORTH:
    			$targetPosition = new Coordinate($this->position->getX(), $this->position->getY() + self::VELOCITY);
    			break;
    		case Direction::EAST:
    			$targetPosition = new Coordinate($this->position->getX() + self::VELOCITY, $this->position->getY());
    			break;
			case Direction::SOUTH:
    			$targetPosition = new Coordinate($this->position->getX(), $this->position->getY() - self::VELOCITY);
    			break;
			case Direction::WEST:
    			$targetPosition = new Coordinate($this->position->getX() - self::VELOCITY, $this->position->getY());
    			break;
    		default:
    			throw new \Exception("Direction: ". $this->currentDirection. " is invalid!");
    			break;
    	}


    	return $targetPosition;
    }

    /**
     * Moves at given coordinate
     *
     * @param Coordinate $targetPosition
     */
    private function move(Coordinate $targetPosition): void 
    {
    	$this->position = $targetPosition;
    	return;
    }

    /**
     * Rotate to the left
     *
	 * @throws \Exception if the given direction is invalid
     */
    private function rotateLeft(): void
    {
    	switch ($this->currentDirection) {
    		case Direction::NORTH:
    			$this->currentDirection = new Direction(Direction::WEST);
    			break;
    		case Direction::EAST:
    			$this->currentDirection = new Direction(Direction::NORTH);
    			break;
			case Direction::SOUTH:
    			$this->currentDirection = new Direction(Direction::EAST);
    			break;
			case Direction::WEST:
    			$this->currentDirection = new Direction(Direction::SOUTH);
    			break;
    		default:
    			throw new \Exception("Direction: ". $this->currentDirection. " is invalid!");
    			break;
    	}
    	return;
    }

    /**
     * Rotate to the right
     *
	 * @throws \Exception if the given direction is invalid
     */
    private function rotateRight(): void
    {
    	switch ($this->currentDirection) {
    		case Direction::NORTH:
    			$this->currentDirection = new Direction(Direction::EAST);
    			break;
    		case Direction::EAST:
    			$this->currentDirection = new Direction(Direction::SOUTH);
    			break;
			case Direction::SOUTH:
    			$this->currentDirection = new Direction(Direction::WEST);
    			break;
			case Direction::WEST:
    			$this->currentDirection = new Direction(Direction::NORTH);
    			break;
    		default:
    			throw new \Exception("Direction: ". $this->currentDirection. " is invalid!");
    			break;
    	}
    	return;
    }

    /**
     * Sets position at given coordinate if there isnt an obstacle or its inside planet boundaries
     *
     * @param Coordinate $coordinate
	 * @throws \Exception if the given direction is invalid
     */
    public function setPosition(Coordinate $coordinate): void
    {
    	if (!$this->radar->scannForObstacles($coordinate) && $this->radar->scannForEdges($coordinate)) {
			$this->position = $coordinate;
		}else {
			throw new \Exception("Cannot be placed at: ". $coordinate. "!");
		}

		return;
    }

    /**
     * @return Coordinate
     */
    public function getPosition(): Coordinate
    {
        return $this->position;
    }

    /**
     * @param Direction $direction
     */
    public function setDirection(Direction $direction): void
    {
		$this->currentDirection = $direction;
		return;
    }

    /**
     * @return Direction
     */
    public function getCurrentDirection(): Direction
    {
        return $this->currentDirection;
    }
}