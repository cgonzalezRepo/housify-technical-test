<?php
declare( strict_types = 1 );

namespace App\Model;

use App\Model\Coordinate;

class Obstacle {

    /**
     * Stores where is the obstacle located
     *
     * @var Coordinate
     */
	private $position;

    /**
     * Obstacle constructor
     * @param Coordinate $coordinate
     */
	public function __construct(Coordinate $coordinate)
	{
		$this->position = $coordinate;
	}

    /**
     * @return string
     */
	public function __toString(): string
    {
        return "Obstacle position: ".$this->position;
    }

    /**
     * @return Coordinate
     */
    public function getPosition(): Coordinate
    {
    	return $this->position;
    }	
}