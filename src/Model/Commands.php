<?php
declare( strict_types = 1 );

namespace App\Model;

use App\Model\Command;

class Commands {

    /**
     * Stores command objects
     *
     * @var array
     */
	private $commands = array();

    /**
     * Commands constructor
     */
	public function __construct(){}

    /**
     * @return string
     */
	public function __toString(): string
    {
        return $this->commands;
    }

    /**
     * Split input string and converts in to a command
     *
     * @param Coordinate $coordinate
     * @return bool
     */
    public function setCommands(string $input): void
    {
    	$splitedInput = str_split($input);

    	foreach ($splitedInput as $character) {
    		$command = new Command($character);
    		array_push($this->commands, $command);
    	}

    	return;
    }

    /**
     * @return array
     */
    public function getCommands(): array
    {
    	return $this->commands;
    }
}