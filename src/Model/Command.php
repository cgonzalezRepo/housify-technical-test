<?php
declare( strict_types = 1 );

namespace App\Model;

class Command {

    /**
     * Command to move forward
     *
     * @var string
     */
	const FORWARD = 'F';

    /**
     * Command to turn left
     *
     * @var string
     */
	const LEFT = 'L';

	/**
     * Command to turn right
     *
     * @var string
     */
	const RIGHT = 'R';


	/**
     * Stores all available commands
     *
     * @var array
     */
	private $availableCommands = [self::FORWARD, self::LEFT, self::RIGHT];

	/**
     * Stores the given command string
     *
     * @var string
     */	
	private $command;

    /**
     * Command constructor
     * @param string $input
	 * @throws \Exception if the command is invalid
     */
	public function __construct(string $input)
	{
		if ($this->isValid($input)) {
			$this->command = $input;
		} else {
			throw new \Exception("Command: ". $input. " is invalid!");
		}
	}

    /**
     * @return string
     */
	public function __toString(): string
    {
        return $this->command;
    }

    /**
     * Check if input string is a valid command
     *
     * @param string $input
     * @return bool
     */
	public function isValid(string $input): bool
    {
    	return in_array($input, $this->availableCommands);
    }
}