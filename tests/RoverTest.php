<?php
declare( strict_types = 1 );

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Model\Planet;
use App\Model\Coordinate;
use App\Model\Rover;
use App\Model\Direction;
use App\Model\Commands;
use App\Model\Obstacle;

final class RoverTest extends TestCase
{

    protected $rover;
    protected $planet;

    protected function setUp(): void
    {
        $maxBoundary = new Coordinate(200,200); 
        $minBoundary = new Coordinate(0,0); 
        $this->planet = new Planet($minBoundary, $maxBoundary, "Mars");
        $this->rover = new Rover($this->planet);
        return;
    }

    public function testRoverAcceptsDirection(): void
    {
        $direction = new Direction("N");
        $this->rover->setDirection($direction);
        $this->assertEquals(Direction::NORTH, $this->rover->getCurrentDirection());
        return;
    }

    public function testRoverAcceptsPosition(): void
    {
        $coordinate = new Coordinate(0,1);
        $this->rover->setPosition($coordinate);
        $this->assertEquals(new Coordinate(0,1), $this->rover->getPosition());
        return;
    }

    public function testRoverCantBePlacedInObstaclePosition(): void
    {
        $this->expectException(\Exception::class);

        $obstacleCoordinates = new Coordinate(0,2);
        $obstacle = new Obstacle($obstacleCoordinates); 
        $this->planet->setObstacle($obstacle);


        $coordinate = new Coordinate(0,2);
        $this->rover->setPosition($coordinate);
        return;
    }

    public function testRoverCantBePlacedInPlanetEdgePosition(): void
    {
        $this->expectException(\Exception::class);

        $coordinate = new Coordinate(-100,0);
        $this->rover->setPosition($coordinate);
        return;
    }

    public function testRoverCanRoteateLeft(): void
    {
        $direction = new Direction("N");
        $this->rover->setDirection($direction);
       
        $commands = new Commands();
        $commands->setCommands("L");
        $this->rover->processCommands($commands->getCommands());

        $this->assertEquals(Direction::WEST, $this->rover->getCurrentDirection());
        return;
    }

    public function testRoverCanRoteateRight(): void
    {
        $direction = new Direction("N");
        $this->rover->setDirection($direction);
       
        $commands = new Commands();
        $commands->setCommands("R");
        $this->rover->processCommands($commands->getCommands());

        $this->assertEquals(Direction::EAST, $this->rover->getCurrentDirection());
        return;
    }


    public function testRoverCanMoveForward(): void
    {
        $direction = new Direction("N");
        $this->rover->setDirection($direction);
        
        $coordinate = new Coordinate(0,1);
        $this->rover->setPosition($coordinate);

        $commands = new Commands();
        $commands->setCommands("F");
        $this->rover->processCommands($commands->getCommands());

        $this->assertEquals(new Coordinate(0,2), $this->rover->getPosition());
        return;
    }
}
