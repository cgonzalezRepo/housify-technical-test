<?php
declare( strict_types = 1 );

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Model\Planet;
use App\Model\Obstacle;
use App\Model\Coordinate;
use App\Model\Radar;

final class RadarTest extends TestCase
{

	protected $rover;
	protected $planet;

    protected function setUp(): void
    {
        $maxBoundary = new Coordinate(200,200); 
        $minBoundary = new Coordinate(0,0); 
        $this->planet = new Planet($minBoundary, $maxBoundary, "Mars");

        $this->radar = new Radar($this->planet);

        return;
    }

    public function testRadarCanFindObstacles(): void
    {
		$obstacleCoordinates = new Coordinate(0,2);
		$obstacle = new Obstacle($obstacleCoordinates); 
		$this->planet->setObstacle($obstacle);

		$output = $this->radar->scannForObstacles($obstacleCoordinates);

        $this->assertEquals(TRUE, $output);
        return;
    }

	public function testRadarCanFindPlanetBoundaries(): void
    {
    	$targetPosition = new Coordinate(-1,0);
		$output = $this->radar->scannForEdges($targetPosition);

        $this->assertEquals(FALSE, $output);
        return;
    }
}
