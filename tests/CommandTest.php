<?php
declare( strict_types = 1 );

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Model\Command;

final class CommandTest extends TestCase
{
    public function testCommandThrowExceptionOnInValidCommand(): void
    {
        $this->expectException(\Exception::class);
        $command = new Command("X");
        return;
    }
}
