<?php
declare( strict_types = 1 );

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Model\Direction;

final class DirectionTest extends TestCase
{
    public function testDirectionThrowExceptionOnInValidDirection(): void
    {
        $this->expectException(\Exception::class);
        $direction = new Direction("X");
        return;
    }
}
