<?php
declare( strict_types = 1 );

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Model\Planet;
use App\Model\Obstacle;
use App\Model\Coordinate;

final class PlanetTest extends TestCase
{
    public function testPlanetCanStoreObstacles(): void
    {
        $maxBoundary = new Coordinate(200,200); 
        $minBoundary = new Coordinate(0,0); 
        $planet = new Planet($minBoundary, $maxBoundary, "Mars");
     
        $obstacleCoordinates = new Coordinate(0,2);
        $obstacle = new Obstacle($obstacleCoordinates); 
        $planet->setObstacle($obstacle);

        $obstacleCoordinates = new Coordinate(10,5);
        $obstacle = new Obstacle($obstacleCoordinates); 
        $planet->setObstacle($obstacle);

        $this->assertCount(2, $planet->getObstacles());
        return;
    }
}
