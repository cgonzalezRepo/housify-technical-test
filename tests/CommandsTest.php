<?php
declare( strict_types = 1 );

namespace App\Test;

use PHPUnit\Framework\TestCase;
use App\Model\Commands;

final class CommandsTest extends TestCase
{
    public function testCommandsCanStoreCommands(): void
    {
        $commands = new Commands();
        $commands->setCommands("FLRFLRFF");

        $this->assertCount(8, $commands->getCommands());
        return;
    }
}
