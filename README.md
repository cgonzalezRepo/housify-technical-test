# Housify technical challenge
_Mars rover mission_

## How to install
Clone this repository and Run composer install

## How to run tests
Use vendor\bin\phpunit to run tests

## Example usage
Run the app with php app.php

### Inputs


```
Mars has an area of a minimum X: 0 Y: 0 and a maximum of X: 200 Y: 200
Do you want to place an obstacle? Y/N:
```
Y

```
Set X coordinate of obstacle:
```
5

```
Set Y coordinate of obstacle:
```
2

```
Obstacle position: X: 5 Y: 2
Do you want to place an obstacle? Y/N:
```
N

```
Set X coordinate of Rover:
```
0

```
Set Y coordinate of Rover:
```
2

```
Set Rover direction [N,S,E,W]:
```
N

```
Actual position: Coordinate: X: 0 Y: 2, is facing: N
Do you want to move Rover? Y/N:
```
Y

```
Write commands to move Rover [F,L,R]:
```
FLRFLRFF

```
Finished position: Coordinate: X: 0 Y: 6, is facing: N
Do you want to move Rover? Y/N:
```
N